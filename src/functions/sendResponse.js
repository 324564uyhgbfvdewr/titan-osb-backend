/**
 * 
 * @memberof module:functions
 * @function sendResponse
 * @description Takes a statuscode, body content and callback, send fires the callback for AWS Lambda
 * @param {string} statusCode - The HTTP status code to return back to the client 
 * @param {object} body - The body of the response
 * @param {callback} callback - The callback passed from AWS Lambda
 * @return {callback} - Returns the callback, which sends back the data to the client
 */
module.exports = (statusCode, body, callback) => {
	const response = {
		statusCode,
		headers: {
			'Content-Type': 'application/json',
			'Access-Control-Allow-Origin': '*',
			'Access-Control-Allow-Headers': 'Authorization',
		},
		body,
	};
	response.body = JSON.stringify(response.body);

	return callback(null, response);
};