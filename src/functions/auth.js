const jwt = require('jsonwebtoken');

const generatePolicy = (principalId, Effect, Resource, context) => ({
	principalId,
	policyDocument: {
		Version: '2012-10-17',
		Statement: [{
			Action: 'execute-api:Invoke',
			Effect,
			Resource,
		}, ],
	},
	context,
});

/**
 * 
 * @memberof  module:functions
 * @function auth
 * @description Authorizes a GET request before going through to the required endpoint from Lambda.
 * @param {object} event - Event details passed in from AWS Lambda.
 * @param {object} context - AWS Lambda uses this parameter to provide details of your Lambda function's execution. {@link https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html|Context Documentation}
 * @param {callback} query - The callback that AWS Lambda uses to feed data back to the client.
 * @return {callback} - Returns the callback, which sends back the policy data determining authorization or denial.
 */
module.exports.handler = (event, context, callback) => {
	const {
		methodArn,
		authorizationToken
	} = event;
	const token = authorizationToken.replace(/Bearer /g, '');
	const secret = process.env.SECRET_KEY;
	const config = {
		maxAge: 86400, // 24 hours
	};

	// verify the token with publicKey and config and return proper AWS policy document
	jwt.verify(token, secret, config, (err, verified) => {
		if (err) {
			console.log('JWT Error', err, err.stack);
			return callback('Server Error');
		} else {
			callback(null, generatePolicy('user', 'Allow', methodArn, {
				token: `custom ${verified.key}`,
			}));
		}
	});
};