const axios = require('axios');
const { apiConfig } = require('./config');

/**
 * 
 * @memberof  module:API
 * @function post
 * @description Handles a POST request from Lambda, and makes the request to the Titan DMS API.
 * @param {string} endpoint - The endpoint that we want to hit
 * @param {string} token - The token we need to authorize our request
 * @param {Object} query - A data object, containing post data
 * @return {Promise<Object>} - Returns a promise with either data retrieved, or an error object
*/
module.exports = (endpoint, token, data) => new Promise((resolve, reject) => {
	// Create endpoint path using config
	let url = `${apiConfig.hostname}:${apiConfig.port}${apiConfig.path}${endpoint}`;

	// Make a requet to the Titan API, passing our JWT and POST body
	axios({
		method: 'POST',
		url,
		headers: {
			'Content-Type': 'application/json',
			'Access-Control-Allow-Origin': '*',
			'Authorization': token,
		},
		data,
	})
		.then(res => resolve(res))
		.catch(err => {
			console.log(err);
			return reject(err);
		});
});