const querystring = require('querystring');
const axios = require('axios');
const { apiConfig } = require('./config');

/**
 * 
 * @memberof  module:API
 * @function get
 * @description Handles a GET request from Lambda, and makes the request to the Titan DMS API.
 * @param {string} endpoint - The endpoint that we want to hit
 * @param {string} token - The token we need to authorize our request
 * @param {Object} query - An optional query object, containing querystring keys and values
 * @return {Promise<Object>} - Returns a promise with either data retrieved, or an error object
*/
module.exports = (endpoint, token, query = false) => new Promise((resolve, reject) => {
	// Create endpoint path using config
	let url = `${apiConfig.hostname}:${apiConfig.port}${apiConfig.path}${endpoint}`;
	// Are there any query parameters? Turn it into a querystring and append to our path
	if (query) url += `?${querystring.stringify(query)}`;

	// Make a requet to the Titan API, passing our JWT
	axios.get(url, {
		headers: {
			'Content-Type': 'application/json',
			'Access-Control-Allow-Origin': '*',
			'Authorization': token,
		}
	})
		.then(res => resolve(res))
		.catch(err => {
			console.log(err);
			return reject(err);
		});
});