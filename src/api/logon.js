const axios = require('axios');
const {
	apiConfig
} = require('./config');

/**
 * 
 * @memberof  module:API
 * @function logon
 * @description Logs in to the Titan DMS API, and returns a token.
 * @return {Promise} - Returns a promise with either the "Ticket" or an error object
*/
module.exports = () => new Promise((resolve, reject) => {

	// Convert our data to a string
	const data = JSON.stringify({
		'DealerId': process.env.DEALER_ID,
		'EmpId': process.env.EMPLOYEE_ID,
		'Password': process.env.EMPLOYEE_PASSWORD,
		'ProviderId': process.env.PROVIDER_ID
	});

	axios.post(`${apiConfig.hostname}:${apiConfig.port}${apiConfig.path}/Logon`, data, {
		headers: {
			'content-type': 'application/json',
			'Content-Length': data.length,
		},
	})
		// Retusns the auth token
		.then(res => resolve(res.data.Ticket))
		.catch((err) => {
			console.log(err);
			console.log(err.message);
			reject(err);
		});
});