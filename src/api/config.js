// Base configuration for each API request to Titan
module.exports.apiConfig = {
	hostname: 'https://interfaces.titandms.net.au',
	port: 8091,
	path: '/api/v1',
};