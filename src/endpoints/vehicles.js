const jwt = require('jsonwebtoken');
const logon = require('../api/logon');
const get = require('../api/get');
const sendResponse = require('../functions/sendResponse');

/**
 * Handles a Vehicles request from the client. It logs into the Titan DMS API, 
 * retrieves a token and use that token to hit the "/Vehicles" endpoint. 
 * If vehicles are found, it will append a JWT to the response for all 
 * subsequent requests.
 * 
 * @memberof  module:endpoints
 * @function vehicles
 * @param	{object} event - Event details passed in from AWS Lambda.
 * @param {object} context - AWS Lambda uses this parameter to provide details of your Lambda function's execution. {@link https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html|Context Documentation}
 * @param {callback} query - The callback that AWS Lambda uses to feed data back to the client.
 * @return {function} - Returns sendResponse() which formats the callback data before sending it back to the client.
 */
module.exports.get = (event, context, callback) => {
	const { queryStringParameters: query } = event;
	let authorizationKey;

	logon()
		.then(authTicket => {
			authorizationKey = authTicket;
			// return promise "get" function with endpoint / ticket (auth) / request url paramaters
			return get('/Vehicles', `custom ${authTicket}`, query);
		})
		.then(res => {
			// vehicle data returned from Titan DMS API
			const data = {
				vehicles: res.data,
			};
			// send back to client empty response as no vehicles found
			if (!res.data.length) return sendResponse(200, data, callback);
			jwt.sign({
				key: authorizationKey
			}, process.env.SECRET_KEY, {
				expiresIn: 86400,
			}, (err, token) => {
				if (err) return sendResponse(400, err, callback);
				// If everything went smoothly, attach token to response and return to client
				data.token = token;
				return sendResponse(200, data, callback);
			});
		})
		.catch(err => sendResponse(err.status, err.statusText, callback));
};