const sendResponse = require('../functions/sendResponse');
const get = require('../api/get');

/**
 * 
 * @memberof  module:endpoints
 * @function get
 * @description Handles a GET request from the client.
 * @param {object} event - Event details passed in from AWS Lambda.
 * @param {object} context - AWS Lambda uses this parameter to provide details of your Lambda function's execution. {@link https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html|Context Documentation}
 * @param {callback} query - The callback that AWS Lambda uses to feed data back to the client.
 * @return {function} - Returns sendResponse() which formats the callback data before sending it back to the client.
 */
module.exports.handler = (event, context, callback) => {
	const {
		requestContext,
		path,
		queryStringParameters,
	} = event;

	get(path, requestContext.authorizer.token, queryStringParameters)
		.then(res => sendResponse(res.status, res.data, callback))
		.catch((err) => sendResponse(err.response.status, {
			Message: err.response.statusText,
		}, callback));
};