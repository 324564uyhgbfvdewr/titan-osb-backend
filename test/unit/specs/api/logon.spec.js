const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');
const logon = require('../../../../src/api/logon');

describe('api: logon()', () => {
	console.log = jest.fn();
	const mockAxios = new MockAdapter(axios);

	afterEach(() => {
		mockAxios.reset();
	});

	it('Returns a ticket upon successful login', () => {
		const mockResponse = {
			Ticket: 'foobar',
		};

		mockAxios.onAny().reply(200, mockResponse);
		return logon()
			.then((res) => {
				expect(res).toEqual(mockResponse.Ticket);
			});
	});

	it('Returns an error if login fails', () => {
		const mockResponse = {
			Error: 'foo',
			Description: 'bar',
		};

		mockAxios.onAny().reply(400, mockResponse);
		return logon()
			.catch((err) => {
				expect(err.response.data).toEqual(mockResponse);
			});
	});
});