const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');
const post = require('../../../../src/api/post');

describe('api: generic [POST]', () => {
	console.log = jest.fn();
	const mockAxios = new MockAdapter(axios);
	const endpoint = '/testing';

	afterEach(() => {
		mockAxios.reset();
	});

	it('Returns data upon successful API request', () => {
		const mockResponse = {
			foo: 'bar',
			bar: 'baz',
		};

		mockAxios.onPost().reply(200, mockResponse);
		return post(endpoint, 'authTokenFooBar')
			.then((res) => {
				expect(res.data).toEqual(mockResponse);
			});
	});

	it('Returns an error if login fails', () => {
		const mockResponse = {
			Error: 'foo',
			Description: 'bar',
		};

		mockAxios.onPost().reply(400, mockResponse);
		return post(endpoint, 'authTokenFooBar')
			.catch((res) => {
				expect(res.response.status).toEqual(400);
				expect(res.response.data).toEqual(mockResponse);
			});
	});
});