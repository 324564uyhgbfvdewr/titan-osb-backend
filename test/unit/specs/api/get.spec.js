const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');
const get = require('../../../../src/api/get');
const {
	apiConfig
} = require('../../../../src/api/config');

describe('api: generic [GET]', () => {
	const mockAxios = new MockAdapter(axios);
	const endpoint = '/testing';

	afterEach(() => {
		mockAxios.reset();
	});

	it('Appends a querystring to the path if one is passed', () => {
		const spy = jest.spyOn(axios, 'get');
		const mockResponse = {
			foo: 'bar',
			bar: 'baz',
		};
		const url = `${apiConfig.hostname}:${apiConfig.port}${apiConfig.path}${endpoint}?query=foo&string=bar`;
		mockAxios.onGet().reply(200, mockResponse);

		return get(endpoint, 'authTokenFooBar', {
			query: 'foo',
			string: 'bar'
		})
			.then(() => {
				expect(spy).toBeCalledWith(url, {
					headers: expect.any(Object),
				});
			});
	});

	it('Returns data upon successful API request', () => {
		const mockResponse = {
			foo: 'bar',
			bar: 'baz',
		};

		mockAxios.onGet().reply(200, mockResponse);
		return get(endpoint, 'authTokenFooBar')
			.then((res) => {
				expect(res.data).toEqual(mockResponse);
			});
	});

	it('Returns an error if login fails', () => {
		const mockResponse = {
			Error: 'foo',
			Description: 'bar',
		};

		mockAxios.onGet().reply(400, mockResponse);
		return get(endpoint, 'authTokenFooBar')
			.catch((res) => {
				expect(res.response.status).toEqual(400);
				expect(res.response.data).toEqual(mockResponse);
			});
	});
});