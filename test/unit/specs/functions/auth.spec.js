jest.mock('jsonwebtoken');

const jwt = require('jsonwebtoken');
const auth = require('../../../../src/functions/auth');
const mockCallback = jest.fn();

describe('functions: auth.js', () => {
	console.error = () => {};
	const event = {
		methodArn: 'methodArn',
		authorizationToken: '123',
	};

	process.env.SECRET_KEY = 'foobar';

	it('verifies the JWT and allows the user to continue', () => {
		jwt.verify.mockImplementation((token, secret, config, cb) => cb(null, 'jwt-foobar'));
		auth.handler(event, null, mockCallback);
		expect(mockCallback).toHaveBeenCalledWith(null, expect.anything());
	});

	it('sees an invalid JWT and rejects the user', () => {
		console.log = jest.fn();
		jwt.verify.mockImplementation((token, secret, config, cb) => cb('Error', false));
		auth.handler(event, null, mockCallback);
		expect(mockCallback).toHaveBeenCalledWith('Server Error');
	});
});