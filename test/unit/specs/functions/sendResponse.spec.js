const sendResponse = require('../../../../src/functions/sendResponse');

const mockCallback = jest.fn();

const statusCode = 200;
const responseBody = { Message: 2 };

describe('functions: sendResponse.js', () => {

	it('sends custom responses via the callback', () => {
		sendResponse(statusCode, responseBody, mockCallback);
		expect(mockCallback).toHaveBeenCalledWith(null, {
			statusCode,
			headers: {
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*',
				'Access-Control-Allow-Headers': 'Authorization',
			},
			body: `${JSON.stringify(responseBody)}` });
	});
});
