jest.mock('../../../../src/api/get');

const awsGet = require('../../../../src/endpoints/get');
const titanGet = require('../../../../src/api/get');
const flushPromises = require('../../mocks/flushPromises');

describe('endpoints: generic [GET]', () => {
	const mockCallback = jest.fn();

	it('Returns a 200 successful response', () => {
		titanGet.mockResolvedValue({
			status: 200,
			data: 'foobar',
		});
		awsGet.handler({
			path: '/foo/bar/baz',
			queryStringParameters: {
				rego: '1234',
				odometer: '1234',
			},
			requestContext: {
				authorizer: {
					token: 'baz',
				},
			},
			headers: {
				Authorization: 'foobar',
			},
		}, '', mockCallback);

		flushPromises().then(() => {
			expect(titanGet).toBeCalled();
			expect(mockCallback).toBeCalledWith(null, {
				'body': '"foobar"',
				'headers': {
					'Content-Type': 'application/json',
					'Access-Control-Allow-Origin': '*',
					'Access-Control-Allow-Headers': 'Authorization',
				},
				'statusCode': 200,
			});
		});
	});

	it('Returns a 400 error if request fails', () => {
		titanGet.mockRejectedValue({
			response: {
				status: 400,
				statusText: 'foobar'
			},
		});
		awsGet.handler({
			path: '/foo/bar/baz',
			queryStringParameters: {
				rego: '1234',
				odometer: '1234',
			},
			requestContext: {
				authorizer: {
					token: 'baz',
				},
			},
			headers: {
				Authorization: 'foobar',
			},
		}, '', mockCallback);

		flushPromises().then(() => {
			expect(titanGet).toBeCalled();
			expect(mockCallback).toBeCalledWith(null, {
				'body': '{"Message":"foobar"}',
				'headers': {
					'Content-Type': 'application/json',
					'Access-Control-Allow-Origin': '*',
					'Access-Control-Allow-Headers': 'Authorization',
				},
				'statusCode': 400,
			});
		});
	});
});