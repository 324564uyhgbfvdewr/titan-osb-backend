jest.mock('jsonwebtoken');
jest.mock('../../../../src/api/logon');
jest.mock('../../../../src/api/get');

const jwt = require('jsonwebtoken');
const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');
const flushPromises = require('../../mocks/flushPromises');
const vehicles = require('../../../../src/endpoints/vehicles');
const logon = require('../../../../src/api/logon');
const get = require('../../../../src/api/get');

describe('endpoints: vehicles.js [GET]', () => {
	const mockCallback = jest.fn();
	const mockAxios = new MockAdapter(axios);

	afterEach(() => {
		mockAxios.reset();
	});

	it('Gets vehicle data, and returns a token if there are vehicles found', () => {
		process.env.SECRET_KEY = 'key-foobar';
		const queryStringParameters = {
			rego: '12345678',
			vin: '12345678912345678',
		};
		jwt.sign.mockImplementation((data, key, config, cb) => cb(null, 'jwt-foobar'));
		logon.mockResolvedValueOnce('authTokenFooBar');
		get.mockResolvedValueOnce({ data: ['foobar'] });
		mockAxios.onGet().reply(200, {
			data: '12345'
		});
		vehicles.get({
			requestContext: {
				authorizer: {
					token: 'foobar',
				},
			},
			queryStringParameters,
		}, '', mockCallback);

		return flushPromises().then(() => {
			expect(logon).toBeCalled();
			expect(mockCallback).toBeCalledWith(null, {
				body: '{"vehicles":["foobar"],"token":"jwt-foobar"}',
				headers: {
					'Content-Type': 'application/json',
					'Access-Control-Allow-Origin': '*',
					'Access-Control-Allow-Headers': 'Authorization',
				},
				statusCode: 200,
			});
		});
	});

	it('Gets vehicle data, but doesn\'t return a token if there are no vehicles found', () => {
		process.env.SECRET_KEY = 'key-foobar';
		const queryStringParameters = {
			rego: '12345678',
			vin: '12345678912345678',
		};
		jwt.sign.mockImplementation((data, key, config, cb) => cb(undefined, 'jwt-foobar'));
		logon.mockResolvedValueOnce('authTokenFooBar');
		get.mockResolvedValueOnce({ data: [] });
		mockAxios.onGet().reply(200, {
			data: '12345'
		});
		vehicles.get({
			requestContext: {
				authorizer: {
					token: 'foobar',
				},
			},
			queryStringParameters,
		}, '', mockCallback);

		return flushPromises().then(() => {
			expect(logon).toBeCalled();
			expect(mockCallback).toBeCalledWith(null, {
				body: '{"vehicles":[]}',
				headers: {
					'Content-Type': 'application/json',
					'Access-Control-Allow-Origin': '*',
					'Access-Control-Allow-Headers': 'Authorization',
				},
				statusCode: 200,
			});
		});
	});

	it('Returns a 400 if JWT signing fails', () => {
		process.env.SECRET_KEY = 'key-foobar';
		const queryStringParameters = {
			rego: '12345678',
			vin: '12345678912345678',
		};
		jwt.sign.mockImplementation((data, key, config, cb) => cb('JWT failed', null));
		logon.mockResolvedValueOnce('authTokenFooBar');
		get.mockResolvedValueOnce({ data: ['foo'] });
		mockAxios.onGet().reply(200, {
			data: '12345'
		});
		vehicles.get({
			requestContext: {
				authorizer: {
					token: 'foobar',
				},
			},
			queryStringParameters,
		}, '', mockCallback);

		return flushPromises().then(() => {
			expect(logon).toBeCalled();
			expect(mockCallback).toBeCalledWith(null, {
				body: '"JWT failed"',
				headers: {
					'Content-Type': 'application/json',
					'Access-Control-Allow-Origin': '*',
					'Access-Control-Allow-Headers': 'Authorization',
				},
				statusCode: 400,
			});
		});
	});

	it('Returns a 400 if login fails', () => {
		logon.mockRejectedValueOnce({
			status: 400,
			statusText: 'auth failure',
			message: 'auth failure'
		});
		vehicles.get({
			requestContext: {
				authorizer: {
					token: 'foobar',
				},
			},
			queryStringParameters: {
				rego: '12345678',
				vin: '12345678912345678',
			},
		}, '', mockCallback);

		return flushPromises().then(() => {
			expect(logon).toBeCalled();
			expect(mockCallback).toBeCalledWith(null, {
				body: '"auth failure"',
				headers: {
					'Content-Type': 'application/json',
					'Access-Control-Allow-Origin': '*',
					'Access-Control-Allow-Headers': 'Authorization',
				},
				statusCode: 400,
			});
		});
	});
});