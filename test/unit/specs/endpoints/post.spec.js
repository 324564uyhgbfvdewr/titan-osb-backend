jest.mock('../../../../src/api/post');

const awsPost = require('../../../../src/endpoints/post');
const titanPost = require('../../../../src/api/post');
const flushPromises = require('../../mocks/flushPromises');

describe('endpoints: generic [POST]', () => {
	const mockCallback = jest.fn();

	it('Returns a 200 successful response', () => {
		titanPost.mockResolvedValue({
			status: 200,
			data: 'foobar',
		});
		awsPost.handler({
			path: '/foo/bar/baz',
			body: '{"rego":"1234","odometer":"1234"}',
			requestContext: {
				authorizer: {
					token: 'baz',
				},
			},
			headers: {
				Authorization: 'foobar',
			},
		}, '', mockCallback);

		flushPromises().then(() => {
			expect(titanPost).toBeCalled();
			expect(mockCallback).toBeCalledWith(null, {
				'body': '"foobar"',
				'headers': {
					'Content-Type': 'application/json',
					'Access-Control-Allow-Origin': '*',
					'Access-Control-Allow-Headers': 'Authorization',
				},
				'statusCode': 200,
			});
		});
	});

	it('Returns a 400 error if request fails', () => {
		titanPost.mockRejectedValue({
			response: {
				status: 400,
				statusText: 'foobar'
			},
		});
		awsPost.handler({
			path: '/foo/bar/baz',
			queryStringParameters: {
				rego: '1234',
				odometer: '1234',
			},
			body: '',
			requestContext: {
				authorizer: {
					token: 'baz',
				},
			},
			headers: {
				Authorization: 'foobar',
			},
		}, '', mockCallback);

		flushPromises().then(() => {
			expect(titanPost).toBeCalled();
			expect(mockCallback).toBeCalledWith(null, {
				'body': '{"Message":"foobar"}',
				'headers': {
					'Content-Type': 'application/json',
					'Access-Control-Allow-Origin': '*',
					'Access-Control-Allow-Headers': 'Authorization',
				},
				'statusCode': 400,
			});
		});
	});
});