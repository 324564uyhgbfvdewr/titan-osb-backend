const path = require('path');

module.exports = {
	cache: false,
	rootDir: path.resolve(__dirname, '../../'),
	moduleFileExtensions: [
		'js',
	],
	collectCoverage: true,
	collectCoverageFrom: [
		'src/**/*.js',
		'!src/api/config.js',
	],
	coverageDirectory: '<rootDir>/test/unit/coverage',
	coverageReporters: [
		'html',
		'text',
	],
	'coverageThreshold': {
		'global': {
			'branches': 100,
			'functions': 100,
			'lines': 100,
			'statements': 100
		}
	}
};