# Titan OSB Backend 🤖
> Titan OSB Backend is "proxy" to the Titan DMS API, with a simple JWT authentication layer. It makes use of AWS services, including EC2, VPC, Lambda and API Gateway.

This project works by routing traffic through a VPC that *Titan DMS* have whitelisted. As we have no method of truly authenticating users, we only allow unauthorised access to the first endpoint, **/Vehicles**. This means that if a user is unable to find a legitimate vehicle registration or VIN that is stored in the database, they cannot access other endpoints. Once a vehicle is returned, we generate a JWT from the token that *Titan DMS* supply. This is passed onto the client, and is used in every subsequent request to the API.

## Prerequisites

### Serverless
You'll need [Serverless][1] installed on your machine to begin.

`npm i serverless -g`

### AWS configuration
You'll also need to have your `.aws` directory set up in the correct place, with your `config` and `credentials` files inside. 

On Windows: `C:\Users\FORENAME.SURNAME\.aws`

On Mac: `~/.aws`


#### config
Your config file needs to contain:
```ini
[default]
output = text
region = eu-west-1
```
#### credentials
Your credentials file needs to contain:
```ini
[default]
aws_access_key_id = REQUEST_THIS
aws_secret_access_key = REQUEST_THIS
```

where `REQUEST_THIS` is the ID and key that you have obtained from the R&D department. You can do this either by speaking to them directly, or by [sending them an e-mail][2].

## Available Endpoints
* /Vehicles?rego=*string*&vin=*string* - Returns an array containing vehicles matching the query
* /Workshops/*{id}*/Capacity

## Serverless
All configuration for serverless deployment is found within the  `serverless.yml` in the root. Here you define any functions or settings that will be used by AWS. An example `serverless.yml` can be found [here][3]. All functions (barring vehiclesGet) need to have an authorizer function against them to prevent abuse.

To deploy your entire serverless configuration: `sls deploy`

To deploy an individual function (faster): `sls deploy function -f funcName`

## Testing
We use [Jest][4] within this project to test our methods. All `spec.js` files are found in `./test`.

Running tests is as simple as `npm run test`. If you want to test a specific file, you can run `npm test fileName`.

We maintain 100% coverage within this project. If the coverage threshold isn't met, commits will fail via a pre-commit hook.


[1]: https://serverless.com/learn/
[2]: mailto:rddev@gforces.co.uk;
[3]: https://serverless.com/framework/docs/providers/aws/guide/serverless.yml/
[4]: https://jestjs.io/docs/en/getting-started.html